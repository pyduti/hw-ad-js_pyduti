function firstTask(){
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const allClients = [...clients1, ...clients2]

function uniquelyLastName(allClients) {
for(let i = 0; i <= allClients.length; i++) {
    for (let m = 0; m <= allClients.length; m++){
        if ( i === m ){
            continue;
        } else if (allClients[i] == allClients[m]) {
            delete allClients.splice(m, 1);
        } else {
            continue;
        }
    }
}
}
uniquelyLastName(allClients)
console.log(allClients)
}
function secondTask(){
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];
const shortInfo = ({ gender, status, ...rest}) => rest
let shortInfoCharacters = new Array;
characters.forEach(value => {
    shortInfoCharacters.push(shortInfo(value));
    })
console.log(shortInfoCharacters)
}
function thirdTask(){
const user1 = {
    name: "John",
    years: 30
  };
const {name,years,isAdmin = false} = user1;
console.log(name, years, isAdmin)
}
function fourthTask(){
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

const satishi = {...satoshi2018, ...satoshi2019, ...satoshi2020}

console.log(satishi)
}
function fifthTask(){
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

  const newBooks = books.concat(bookToAdd);
  console.log(newBooks)
}
function sixthTask(){
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}
const advEmployee = {...employee, age: 18, salary: 0};
console.log(advEmployee)
}
function seventhTask(){
    const array = ['value', () => 'showValue'];

    let [value, showValue] = array;
    
    alert(value); // должно быть выведено 'value'
    alert(showValue());  // должно быть выведено 'showValue'
}
document.querySelector('.btn-box').addEventListener('click', (e) => {
    switch (e.target.className) {
        case 'btn-box__btn first': firstTask();break;
        case 'btn-box__btn second': secondTask();break;
        case 'btn-box__btn third': thirdTask();break;
        case 'btn-box__btn fourth': fourthTask();break;
        case 'btn-box__btn fifth': fifthTask();break;
        case 'btn-box__btn sixth': sixthTask();break;
        case 'btn-box__btn seventh': seventhTask();break;
    }
})

